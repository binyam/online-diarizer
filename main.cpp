#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/background_segm.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/objdetect/objdetect.hpp>
//#include <eigen3/Eigen/Array>
#include <eigen3/Eigen/Dense> //should be included before <opencv2/core/eigen.hpp>
#include <opencv2/core/eigen.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/math/distributions/poisson.hpp>
#include <boost/math/distributions/normal.hpp>
#include <boost/math/distributions/gamma.hpp>
#include <boost/math/distributions/lognormal.hpp>
#include <sstream>
#include <fstream>
#include <vector>
#include <queue>

#include "mhi.h"
#include "skmeans.h"


using namespace std;
using namespace cv;
using namespace Eigen;
using boost::math::poisson_distribution;
using boost::math::normal_distribution;
using boost::math::gamma_distribution;
using boost::math::lognormal_distribution;

const char* usage = "usage: ./online-diarizer file [-c camera_number] [-p position] [-d frame_rate] [-s mhi_size]";

double parse_position(int argc,char** argv)
{
    for(int i = 1; i < argc; i++){
        string str = argv[i];
        if(str.length() != 2)continue;
        if(strcmp(str.c_str(),"-p") == 0){
            if(argc > i+1) return atof(argv[i+1]);
        }
    }
    return 0.0;
}

char* parse_filename(int argc,char** argv)
{
    for(int i = 1; i < argc; i++){
        string str = argv[i];
        if(str.length() != 2) continue;
        if(strcmp(str.c_str(),"-f") == 0){
            if(argc > i+1) return argv[i+1];
        }
    }
    return "features.txt";
}

int parse_camera(int argc,char** argv)
{
    for(int i = 1; i < argc; i++){
        string str = argv[i];
        if(str.length() != 2)continue;
        if(strcmp(str.c_str(),"-c") == 0){
            if(argc > i+1) return atoi(argv[i+1]);
        }
    }
    return 1;
}

int parse_delay(int argc,char** argv)
{
    for(int i = 1; i < argc; i++){
        string str = argv[i];
        if(str.length() != 2)continue;
        if(strcmp(str.c_str(),"-d") == 0){
            if(argc > i+1) return atoi(argv[i+1]);
        }
    }
    return 0;
}

double parse_mhi_size(int argc,char** argv)
{
    for(int i = 1; i < argc; i++){
        string str = argv[i];
        if(str.length() != 2)continue;
        if(strcmp(str.c_str(),"-s") == 0){
            if(argc > i+1) return atof(argv[i+1]);
        }
    }
    return 1;
}

//finds the speaker with the highest probability of activity
vector <double> maximum_prob(queue <vector <double> > probs){
    vector<double> product;
    for (int i = 0; i < 4;i++)
        product.push_back(1.0);

    for (int i = 0; i < probs.size(); i++){
        vector <double> prob = probs.front();
        for (int j = 0; j < prob.size(); j++)
            product[j] *= prob[j];
        }
    return product;
}

int main(int argc, char *argv[])
{
    VideoCapture cap[4];
    VideoWriter writer;
    string wname = "Who is speaking?";
    namedWindow(wname,0);
    int num_cameras = parse_camera(argc,argv);
    double position = parse_position(argc,argv);
    double hsize = parse_mhi_size(argc,argv);
    char* fname = parse_filename(argc,argv);

    if (argc == 1)
        cap[0].open(0);
    else{
        switch (num_cameras){
        case 1:
            cap[0].open(argv[1]);
            cap[0].set(CV_CAP_PROP_POS_FRAMES, position);
            break;
         case 4:
            string name = argv[1];
            for(int i = 0; i < num_cameras; i++){
                string i_str = boost::lexical_cast<string>(i+1);
                cap[i].open((name + i_str + ".avi").c_str());
                cap[i].set(CV_CAP_PROP_POS_FRAMES, position);
            }
            break;
        }

    }

    double rate = cap[0].get(CV_CAP_PROP_FPS);

    int delay = parse_delay(argc,argv);
    delay = (delay == 0)?1000/rate:delay;

    if(!cap[0].isOpened())
        return -1;
    int width = static_cast<int>(cap[0].get(CV_CAP_PROP_FRAME_WIDTH));
    int height = static_cast<int>(cap[0].get(CV_CAP_PROP_FRAME_HEIGHT));


    Mat motion, dst_motion;;
    MHI mhi(hsize);

    ofstream fout(fname);

    Mat frame[4];
    Mat big_frame = Mat(height,4*width,CV_8UC3);
    Mat big_frame2 = Mat(2*height,4*width,CV_8UC3);
    writer.open("speaker_diarization_demo.avi",
                CV_FOURCC('M','J','P','G') ,
                25,
                Size(big_frame2.cols,big_frame2.rows));
    Mat motion3U;


    int NUM = 4;
    MatrixXd P = MatrixXd::Constant(NUM,1,1.0/4.0);

    // 2x|speakers| matrix, probability of each speaking or not speaking
    MatrixXd P_all = MatrixXd::Constant(2,NUM,0.5);

    double a = 0.9;
    //D is a transition matrix between speakers
    MatrixXd D = MatrixXd::Identity(NUM,NUM)*a + MatrixXd::Constant(NUM,NUM,(1.0-a)/(NUM-1)) -
            MatrixXd::Identity(NUM,NUM)*(1.0-a)/(NUM-1);

    // probability of speaking and not-speaking for all speakers
    MatrixXd D_each = MatrixXd::Identity(2,2)*a + MatrixXd::Constant(2,2,1.0-a) -
            MatrixXd::Identity(2,2)*(1.0-a);


    MatrixXd M_mhi = MatrixXd::Zero(NUM,1);
    MatrixXd M_mei = MatrixXd::Zero(NUM,1);

    MatrixXd I;

    bool stop = false;
    //poisson_distribution<> pdist(50000);
    //normal_distribution<> ndist(4e5,1e5);

    gamma_distribution<> speaking_gamma_dist(1.5,8687.4);
    gamma_distribution<> non_speaking_gamma_dist(0.115,2.3247e4);

    lognormal_distribution<> logn_dist(9.0,1.0);
    bool pause  = false;
    bool must_break = false;
    while(!stop){
        if(!pause){
            for(int i = 0; i < num_cameras; i++){
                cap[i] >> frame[i];
                if(frame[i].empty()){
                    must_break = true; break;
                 }
                frame[i].copyTo(big_frame(Rect(i*width,0,width,height)));
            }
            if(must_break) break;
            mhi.update_mhi(big_frame, motion, 50 );

            cv2eigen(motion,I);
            double mhi_total = I.sum();


            threshold(motion,dst_motion,1,1,THRESH_BINARY);
            cv2eigen(dst_motion,I);
            double mei_total = I.sum();

            for(int i = 0; i < NUM; i++){
                Mat mask(motion.size(), CV_8UC1, Scalar::all(0));
                Mat roi = motion(Rect(i*width,0,width,height));
                // Mat roi = mask(Rect(i*width,0,width,height));
                //roi.setTo(Scalar::all(255));

                cv2eigen(roi,I);
                double mhi_sum = I.sum();
                M_mhi(i) = (mhi_sum + 1);// (mhi_total + NUM);

                Mat dst_roi = dst_motion(Rect(i*width,0,width,height));;
                cv2eigen(dst_roi,I);
                double mei_sum = I.sum();
                M_mei(i) = (mei_sum + 1);// (mei_total + NUM);
                fout << mhi_sum << ',' << mei_sum << ',';

            }

            //used to be P = D*P; but now
            P_all = D_each*P_all;

            for(int i = 0; i < NUM; i++){
                //P(i,0) = P(i,0) * M_mhi(i,0);// * M_mei(i,0);
                //P(i,0) *= cdf(logn_dist, M_mei(i,0));
                //cout << pdf(logn_dist, M_mei(i,0)) << ',' << P(i,0) << ';';
                //P_all(0,i) *= pdf(logn_dist, M_mei(i,0));
                P_all(0,i) *= pdf(speaking_gamma_dist, M_mei(i,0));
                P_all(1,i) *= pdf(non_speaking_gamma_dist, M_mei(i,0));

            }

            for(int i = 0; i < NUM; i++){
               double total = P_all(0,i) + P_all(1,i);
               P_all(0,i) /= total;
               P_all(1,i) /= total;
            }
        }

        //cout << P_all << "\t";
        int active_speaker, row; //row represents index of the active speaker
        double maxVal = P_all.maxCoeff(&row,&active_speaker);



        //cout << active_speaker*row << endl;

        active_speaker*= row;
        string text = "Speaking";
        int fontFace = FONT_HERSHEY_SCRIPT_SIMPLEX;
        double fontScale = 2;
        int thickness = 3;
        Size textsize = getTextSize("Speaking", FONT_HERSHEY_SCRIPT_SIMPLEX, fontScale, thickness,0);
        double mx = P_all(0,0);
        int arg_mx = 0;
        for(int i = 0; i < NUM; i++){
            if (P_all(0,i) > mx){
                mx = P_all(0,i);
                arg_mx = i;
            }
            if (P_all(0,i) < 0.){
                if (i < NUM-1)
                    fout << P_all(0,i) << ",";
                else fout << P_all(0,i) << endl;
            }
            else{
                if (i < NUM-1) fout << P_all(0,i) << ",";
                else fout << P_all(0,i) << endl;

                line(big_frame,Point(i*width+50,big_frame.rows), Point(i*width+50,big_frame.rows-P_all(0,i)*big_frame.rows),Scalar(0,255,255),10);
            }
        }
        assert(active_speaker < NUM && active_speaker >= 0);
        int i = arg_mx;
        Point textOrg(i*width + (width-textsize.width)/2, 50);
        putText(big_frame,text,textOrg, fontFace, fontScale, Scalar(0,255,255), thickness,8);

        //threshold(motion,motion,1,255,THRESH_BINARY);
        Mat tmp;

        threshold(motion,tmp,1,255,THRESH_BINARY);
        cvtColor(motion,motion3U,CV_GRAY2BGR);
        big_frame.copyTo(big_frame2(Rect(0,0,4*width,height)));
        motion3U.copyTo(big_frame2(Rect(0,height,4*width,height)));
        imshow(wname,big_frame2);
        writer << big_frame2;

        char key;
        key = waitKey(delay);
        if(key=='q' || (int)key == 27) stop = true;
        if(key == ' ') pause = !pause;

    }

    return 0;
}


int main_sign(int argc,char* argv[]){
    VideoCapture cap;
    int camera = parse_camera(argc,argv);
    double rate = cap.get(CV_CAP_PROP_FPS);
    double position = parse_position(argc,argv);
    double hsize = parse_mhi_size(argc,argv);
    char* fname = parse_filename(argc,argv);
    int delay = parse_delay(argc,argv);
    delay = (delay == 0)?1000/rate:delay;

    if (argc == 1)
        cap.open(0);
    else {
        cap.open(argv[1]);
        cap.set(CV_CAP_PROP_POS_FRAMES, position);
    }

    MHI mhi(hsize);

    bool stop = false;
    Mat  frame, motion, mei_motion;


    int width = static_cast<int>(cap.get(CV_CAP_PROP_FRAME_WIDTH));
    int height = static_cast<int>(cap.get(CV_CAP_PROP_FRAME_HEIGHT));


    ofstream fout(fname);
    Mat cum = Mat::zeros(height,width,CV_64FC1);
    Mat cum_image;

    vector<Point2f> ini_centers,centers;
    ini_centers.push_back(Point2f(0,0));
    ini_centers.push_back(Point2f(width,1));
    skmeans sk(ini_centers);
    sk.set_map_size(height,width);
    centers = sk.get_centers();


    bool pause = false;
    int NUM(2);
    MatrixXd P = MatrixXd::Constant(NUM,1,1.0/NUM);

    double a(0.9);
    MatrixXd D = MatrixXd::Identity(NUM,NUM)*a + MatrixXd::Constant(NUM,NUM,(1.0-a)/(NUM-1)) -
            MatrixXd::Identity(NUM,NUM)*(1.0-a)/(NUM-1);


    MatrixXd M_mhi = MatrixXd::Zero(NUM,1);
    MatrixXd M_mei = MatrixXd::Zero(NUM,1);

    MatrixXd I;
    int lambda_mei(16e3);
    int lambda_mhi(1.5e6);
    poisson_distribution<> pdist(lambda_mei);
    normal_distribution<> ndist(4e5,1e5);
    gamma_distribution<> speaking_gamma_dist(1.5562,1.3354e4);
    gamma_distribution<> non_speaking_gamma_dist(0.3483,2.00098e4);

    lognormal_distribution<> logn_dist(14.0,1.0);

    while(!stop){
        if (!pause) {
            cap >> frame;
            blur( frame, frame, Size(3,3) );

            if(frame.empty())
                break;

            mhi.update_mhi(frame, motion, 50);

            cv2eigen(motion,I);
            //double mhi_total = I.sum();


            threshold(motion,mei_motion,1,1,THRESH_BINARY);
            cv2eigen(mei_motion,I);
            //double mei_total = I.sum();

            Mat submotion;
            for(int i = 0; i < NUM; i++){
                Mat mask(motion.size(), CV_8UC1, Scalar::all(0));
                sk.get_mask(i+1,mask);
                bitwise_and(motion,mask,submotion);

                cv2eigen(submotion,I);
                double mhi_sum = I.sum();
                M_mhi(i) = mhi_sum + 1; //(mhi_sum + 1)/ (mhi_total + NUM);

                bitwise_and(mei_motion,mask,submotion);
                cv2eigen(submotion,I);
                double mei_sum = I.sum();
                M_mei(i) = mei_sum + 1; //(mei_sum + 1)/ (mei_total + NUM);
            }

            P = D*P;

            for(int i = 0; i < NUM; i++){
                //cout << log(M_mei(i,0)) << ',';
                //P(i,0) = P(i,0) * pdf(pdist, M_mei(i,0));// * M_mei(i,0); //poisson
                //P(i,0) *= cdf(pdist, M_mei(i,0));// * M_mei(i,0);
                //P(i,0) *= cdf(gdist, M_mei(i,0)); cout << cdf(gdist, M_mei(i,0)) << ',';
               // P(i,0) *= cdf(logn_dist, M_mhi(i,0));
                P(i,0) *= pdf(speaking_gamma_dist, M_mei(i,0));
                P(i,0) /= pdf(non_speaking_gamma_dist, M_mei(i,0));
                //cout << cdf(logn_dist, M_mei(i,0)) << ',' <<  P(i,0) << ';';
                if (i < NUM-1)
                    fout << M_mei(i,0) << ',' << M_mhi(i,0) << ',';
                else fout << M_mei(i,0) << ',' << M_mhi(i,0) << endl;
            }

            double total = P.sum();
            for(int i = 0; i < NUM; i++){
                P(i,0) = P(i,0)/total;
//                if (i < NUM-1)
//                    cout << P(i,0) << ',';
//                else cout << P(i,0) << endl;
            }

            int active_signer, col; //row represents index of the active speaker
            //double minProb = P.maxCoeff(&active_signer,&col);

            string text = "Signing";
            int fontFace = FONT_HERSHEY_SCRIPT_SIMPLEX;
            double fontScale = 2;
            int thickness = 3;
            Size textsize = getTextSize("Speaking", FONT_HERSHEY_SCRIPT_SIMPLEX, fontScale, thickness,0);
            //sk.get_mask(active_signer+1,mask);

           // poisson_distribution<int> pdist(exp);

//            for(int i = 0; i < NUM; i++){
//                if (P(i,0) < 0.5){
//                    if (i < NUM-1)
//                        fout << P(i,0) << ',';
//                    else fout << P(i,0) << endl;
//                }
//                else{
//                    if (i < NUM-1) fout << P(i,0)<< ',';
//                    else fout << P(i,0) << endl;
//                    Point pos = sk.get_centerByIdx(i);
//                    Point textOrg(Point(pos.x-textsize.width/2*(1 - i) ,pos.y));
//                    putText(frame,text,textOrg, fontFace, fontScale, Scalar(0,255,255), thickness,8);
//                }
//            }


//            for (int i = 0; i < NUM; i++){
//                line(frame,Point(i*width+50,big_frame.rows),
//                 Point(i*width+50,big_frame.rows-P(i)*100),Scalar(0,255,255),10);
//            }

            //fout << active_speaker << endl;
            imshow("Motion History Image",motion);

            //if (countNonZero(motion) == height*width)
            threshold(motion,motion,1,1,THRESH_BINARY);
            sk.predict_update(motion);
            centers = sk.get_centers();


            motion.convertTo(motion,CV_64FC1);
            cum += motion;

            double maxVal,minVal;
            minMaxLoc(cum, &minVal, &maxVal);
            cum.convertTo(cum_image,CV_8U,255.0/(maxVal-minVal), -255.0*minVal/(maxVal-minVal));

        }

        imshow("Original Frame",frame);

        Mat dst = Mat::zeros(frame.size(), CV_8UC3);
        Mat mask;
        Mat result;
        for (int i = 0; i < centers.size(); i++){
            sk.get_mask(i+1,mask);
            bitwise_and(cum_image,mask,result);

            Point p1 = centers[i];
            int len = 25;
            line(result,Point(p1.x-len,p1.y),Point(p1.x+len,p1.y),Scalar::all(0),5);
            line(result,Point(p1.x,p1.y-len),Point(p1.x,p1.y+len),Scalar::all(0),5);
            circle(result,centers[i],2*len,Scalar(255,255,255),5);
            insertChannel(result, dst, i);
        }
        imshow("All motions",dst);

        char key;
        key = waitKey(delay);
        if(key=='q' || (int)key == 27) stop = true;
        if(key == ' ') pause = !pause;
    }
}


void write_image(ostream& fout,Mat& image){
    for (int row = 0; row < image.rows-1; row++){
        uchar* p = image.ptr(row);
        for(int col = 0; col < image.cols; ++col) {
            fout << int(*p++)  << ',';
        }
    }

    int row = image.rows-1;
    uchar* p = image.ptr(row);
    for(int col = 0; col < image.cols-1; ++col){
        fout << int(*p++)  << ',';
    }
    fout << int(*p++) << endl;

}


int main_shown_to_sebastian(int argc, char *argv[])
{
    VideoCapture cap,cap2,cap3,cap4;
    int camera = parse_camera(argc,argv);
    double rate = cap.get(CV_CAP_PROP_FPS);
    double position = parse_position(argc,argv);
    double hsize = parse_mhi_size(argc,argv);
    char* fname = parse_filename(argc,argv);
    int delay = parse_delay(argc,argv);
    delay = (delay == 0)?1000/rate:delay;

    if (argc == 1)
        cap.open(0);
    else{
        switch (camera){
        case 1:
            cap.open(argv[1]);
            cap.set(CV_CAP_PROP_POS_FRAMES, position);
            break;
         case 4:
            string name = argv[1];
            cap.open((name + "1.avi").c_str());
            cap2.open((name + "2.avi").c_str());
            cap3.open((name + "3.avi").c_str());
            cap4.open((name + "4.avi").c_str());
            cap.set(CV_CAP_PROP_POS_FRAMES, position);
            cap2.set(CV_CAP_PROP_POS_FRAMES, position);
            cap3.set(CV_CAP_PROP_POS_FRAMES, position);
            cap4.set(CV_CAP_PROP_POS_FRAMES, position);
            break;
        }

    }

    if(!cap.isOpened())
        return -1;

    int width = static_cast<int>(cap.get(CV_CAP_PROP_FRAME_WIDTH));
    int height = static_cast<int>(cap.get(CV_CAP_PROP_FRAME_HEIGHT));


    Mat motion;
    MHI mhi(hsize);

    bool stop = false;

    ofstream fout;

    fout.open(fname);

    Mat vect;
    Mat frame,frame2,frame3,frame4;
    Mat big_frame = Mat(height,4*width,CV_8UC3);

    queue <vector<double> > probs;
    int history_size = 5;

    while(!stop){
        cap >> frame;
        cap2 >> frame2;
        cap3 >> frame3;
        cap4 >> frame4;
        if(frame.empty() || frame2.empty() || frame3.empty() ||frame4.empty()) break;

        frame.copyTo(big_frame(Rect(0,0,width,height)));
        frame2.copyTo(big_frame(Rect(width,0,width,height)));
        frame3.copyTo(big_frame(Rect(2*width,0,width,height)));
        frame4.copyTo(big_frame(Rect(3*width,0,width,height)));
        mhi.update_mhi(big_frame, motion, 50 );

 //       reduce(motion,vect,0,CV_REDUCE_MAX);

//        vector <int> speakers; // gesture activity probability
//        vector<double> prob;

//        int sp1 = countNonZero(vect(Rect(0,0,width,1)));
//        speakers.push_back(sp1);
//        int sp2 = countNonZero(vect(Rect(width,0,width,1)));
//        speakers.push_back(sp2);
//        int sp3 = countNonZero(vect(Rect(2*width,0,width,1)));
//        speakers.push_back(sp3);
//        int sp4 = countNonZero(vect(Rect(3*width,0,width,1)));
//        speakers.push_back(sp4);

        vector <int> speakers; // gesture activity probability
        vector<double> prob;

//        int sp1 = countNonZero(motion(Rect(0,0,width,height)));
//        speakers.push_back(sp1);
//        int sp2 = countNonZero(motion(Rect(width,0,width,height)));
//        speakers.push_back(sp2);
//        int sp3 = countNonZero(motion(Rect(2*width,0,width,height)));
//        speakers.push_back(sp3);
//        int sp4 = countNonZero(motion(Rect(3*width,0,width,height)));
//        speakers.push_back(sp4);
//        double total = static_cast<double> (sp1+sp2+sp3+sp4);

        double sp1 = sum(motion(Rect(0,0,width,height))).val[0];
        speakers.push_back(sp1);
        double sp2 = sum(motion(Rect(width,0,width,height))).val[0];
        speakers.push_back(sp2);
        double sp3 = sum(motion(Rect(2*width,0,width,height))).val[0];
        speakers.push_back(sp3);
        double sp4 = sum(motion(Rect(3*width,0,width,height))).val[0];
        speakers.push_back(sp4);

        double total =sp1+sp2+sp3+sp4;


        for (int i = 0; i < speakers.size(); i++)
            prob.push_back(speakers[i]/total);

        if (probs.size() < history_size)
            probs.push(prob);
        else{
            probs.pop();
            probs.push(prob);
        }

        vector <double> product_prob = maximum_prob(probs);
        double total_probs = 0.0;
        for(int i = 0; i < product_prob.size(); i++){
           total_probs +=  product_prob[i];
        }
        for(int i = 0; i < product_prob.size(); i++){
           product_prob[i] /= total_probs;
        }

        double max_value = *max_element(product_prob.begin(),product_prob.end());

        int active_speaker;
        for (int i = 0; i < 4; i++){
            if (product_prob[i] == max_value){
                active_speaker = i;
                break;
            }
        }
        //write_image(fout,vect);

        int fn = static_cast<int>(cap.get(CV_CAP_PROP_POS_FRAMES));
        fout << fn << ',' << active_speaker <<  endl;

        string text = "Speaking";
        int fontFace = FONT_HERSHEY_SCRIPT_SIMPLEX;
        double fontScale = 2;
        int thickness = 3;
        Size textsize = getTextSize("Speaking", FONT_HERSHEY_SCRIPT_SIMPLEX, fontScale, thickness,0);
        Point textOrg(active_speaker*width + (width-textsize.width)/2, 50);
        putText(big_frame,text,textOrg, fontFace, fontScale, Scalar(0,255,255), thickness,8);

        for (int i = 0; i < product_prob.size(); i++){
            line(big_frame,Point(i*width+50,big_frame.rows),
             Point(i*width+50,big_frame.rows-product_prob[i]*100),Scalar(0,255,255),10);
        }

      //  threshold(motion,motion,1,255,THRESH_BINARY);
        imshow("Motion History Image",motion);
        imshow("Original Frame",big_frame);
        if(waitKey(delay)=='q') stop = true;

    }
    //cout << cap.get(CV_CAP_PROP_POS_FRAMES) - position << endl;
    return 0;
}

