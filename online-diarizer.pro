#-------------------------------------------------
#
# Project created by QtCreator 2013-10-09T16:56:15
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = online-diarizer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    mhi.cpp \
    skmeans.cpp

HEADERS += \
    mhi.h \
    skmeans.h

INCLUDEPATH += /usr/local/include
LIBS += -L/usr/local/lib \
-lopencv_core \
-lopencv_highgui \
-lopencv_imgproc \
-lopencv_features2d \
-lopencv_ml \
-lopencv_video \
-lopencv_objdetect

