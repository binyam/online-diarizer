#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/background_segm.hpp>
#include <opencv2/video/video.hpp>
#include <fstream>

using namespace std;
using namespace cv;

#include "mhi.h"

MHI::MHI()
{
    _MHI_DURATION = 1;
    _N = 5;
    _last = 0;
    _buf.resize(_N);
}

MHI::MHI (double du):
    _MHI_DURATION(du), _N(2)
{
 _last = 0;
 _buf.resize(_N);
}

void MHI::reset(){
    _last = 0;
    _buf.resize(0);
    _buf.resize(_N);
}

void  MHI::update_mhi( const Mat& img, Mat& dst, int diff_threshold )
{
    double timestamp = (double)clock()/CLOCKS_PER_SEC; // get current time in seconds
    int idx1 = _last;
    int idx2;

    Mat silh;

    cvtColor(img, _buf[_last], CV_BGR2GRAY );
    idx2 = (_last + 1) % _N; // index of (last - (N-1))th frame
    _last = idx2;

    if(_buf[idx1].size() != _buf[idx2].size() )
        silh = Mat::zeros(img.size(), CV_8U);
    else{
        absdiff(_buf[idx1], _buf[idx2], silh); // get difference between frames
        //_mog(_buf[idx1],silh,0.1);
    }


    threshold( silh, silh, diff_threshold, 1, CV_THRESH_BINARY);
    if(_mhi.empty() )
        _mhi = Mat::zeros(silh.size(), CV_32F);

    updateMotionHistory( silh, _mhi, timestamp, _MHI_DURATION ); // update MHI

    _mhi.convertTo(dst, CV_8U, 255./_MHI_DURATION, (min(0.0,_MHI_DURATION - timestamp))*255./_MHI_DURATION);

}
