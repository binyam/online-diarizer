#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/background_segm.hpp>
#include <opencv2/video/video.hpp>
#include <fstream>

using namespace std;
using namespace cv;

#ifndef MHI_H
#define MHI_H

class MHI{
public:
    MHI();
    MHI(double du);
    void reset();
    void  update_mhi( const Mat& img, Mat& dst, int diff_threshold);
private:
    vector<Mat> _buf;
    BackgroundSubtractorMOG _mog;
    int _last;
    int _N;
    Mat _mhi;
    double _MHI_DURATION;
};

#endif
