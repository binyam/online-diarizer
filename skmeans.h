#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/background_segm.hpp>
#include <opencv2/video/video.hpp>
#include <fstream>

using namespace cv;
using namespace std;

#ifndef SKMEANS_H
#define SKMEANS_H

class skmeans
{
public:
    skmeans(vector<Point2f> ini_centers);
    void predict_update(vector<Point2f>& points);
    void predict_update(Mat& motionPoints);
    int predict(Point2f p);
    vector<Point2f> get_centers();
    void set_map_size(int rows, int cols);
    void get_mask(int cIdx,Mat& mask);
    Point get_centerByIdx(int cIdx);

private:
    vector<int> _count;
    vector<Point2f> _centers;
    Mat _map;
    //Mat covars;
};

#endif // SKMEANS_H
