#include "skmeans.h"
#include <iostream>


skmeans::skmeans(vector<Point2f> ini_centers)
{
   _centers = ini_centers;
   for(int i = 0; i < ini_centers.size(); i++){
       _count.push_back(0);
   }
}

void skmeans::set_map_size(int rows, int cols){
    _map = Mat::zeros(rows,cols,CV_8UC1);
}

int skmeans::predict(Point2f p){
    double min = 1e10;
    int minIdx = 0;
    for (int i = 0; i < _centers.size(); i++){
        double distance = (_centers[i].x - p.x)*(_centers[i].x - p.x) +
                        (_centers[i].y - p.y)*(_centers[i].y - p.y);

        if (distance < min){
            min = distance;
            minIdx = i;
        }
    }
    return minIdx;
}

vector<Point2f> skmeans::get_centers() {
    return _centers;
}
Point skmeans:: get_centerByIdx(int cIdx){
    assert(cIdx < _centers.size() && cIdx >= 0);
    return Point((int)_centers[cIdx].x,(int)_centers[cIdx].y);
}

void skmeans::get_mask(int cIdx, Mat &mask){
    mask = _map == cIdx;
    //compare(_map,cIdx,mask,CMP_EQ);
}

void skmeans::predict_update(vector<Point2f>& points){

    for(int i = 0; i < points.size(); i++){
        Point2f p = points[i];
        int idx = predict(p);
        _map.at<uchar>((int)p.x,(int)p.y) = idx + 1;
        _count[idx] += 1;
        _centers[idx] += (1/_count[idx])*( p - _centers[idx]);
    }
}

void skmeans::predict_update(Mat& image){

    for (int row = 0; row < image.rows; row++){
        for(int col = 0; col < image.cols; ++col) {
            if((int)image.at<uchar>(row,col) == 1){
                Point2f p = Point2f((double)col,(double)row);
                int idx = predict(p);
                _map.at<uchar>(row,col) = idx + 1;
                _count[idx] += 1;
                _centers[idx] = _centers[idx] + 1.0/_count[idx]*(p -_centers[idx]) ;
                //cout << _centers << endl;
            }
        }
    }
}
